import {FETCH_IMAGE_SUCCESS, TOGGLE_MODAL} from "../actions/actionTypes";

const initialState = {
  images: [],
  showModalLogin: false,
  currentImage:null
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_IMAGE_SUCCESS:
        return {...state, images: action.images};
      case TOGGLE_MODAL:
          return {...state, showModalLogin: !state.showModalLogin, currentImage:action.image};
    default:
      return state;
  }
};

export default reducer;
