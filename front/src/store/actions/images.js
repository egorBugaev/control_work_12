import axios from '../../axios-api';
import {push} from "react-router-redux";
import {CREATE_IMAGE_SUCCESS, FETCH_IMAGE_SUCCESS, TOGGLE_MODAL} from "./actionTypes";

export const fetchImagesSuccess = images => {
  return {type: FETCH_IMAGE_SUCCESS, images: images};
};
export const fetchImagesIdSuccess = images => {
  return {type: FETCH_IMAGE_SUCCESS, images: images};
};

export const fetchImages = () => {
  return dispatch => {
    axios.get('/images').then(
      response => dispatch(fetchImagesSuccess(response.data))
    );
  }
};

export const toggleModalLogin = (image) => {
    return (dispatch) => {
        dispatch(toggleModal(image));
    };
};

export const toggleModal = (image) => {
    return {type: TOGGLE_MODAL,image};
};

export const fetchImagesId = (id) => {
    return dispatch => {
    axios.get(id).then(
      response => {
          dispatch(fetchImagesIdSuccess(response.data))
      }
    );
  }
};

export const deleteImage = (id) => {
    return dispatch => {
        axios.delete(`/images/${id}`).then(
            () => {
                dispatch(fetchImages())
            }
        );

    }
};
export const createProductSuccess = () => {
  return {type: CREATE_IMAGE_SUCCESS};
};

export const createImage = imageData => {
  return dispatch => {
    return axios.post('/images', imageData).then(
      response => {
        dispatch(createProductSuccess());
        dispatch(push('/'));
      }
    );
  };
};
