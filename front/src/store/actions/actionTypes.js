export const FETCH_IMAGE_SUCCESS = 'FETCH_IMAGE_SUCCESS';
export const CREATE_IMAGE_SUCCESS = 'CREATE_IMAGE_SUCCESS';

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';




