import React from 'react';
import {Button, Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';


const ImageListItem = props => {

   let image = config.apiUrl + '/uploads/' + props.image;


  return (
    <Panel style={{width: '28%',
    float: 'left', marginRight:'15px'}}>
      <Panel.Body>
        <Image
          style={{width: '100px', marginRight: '10px'}}
          src={image}
          thumbnail
        />
        <a  onClick={()=>props.modal(image)} style={{marginRight: '30px'}}>
          {props.title}
        </a>
          <Link to={'/users/' + props.owner._id}>
               {props.owner.username}
          </Link>
          {props.user && props.user._id === props.owner._id &&
              <Button onClick={()=>props.delete(props.id)} bsStyle="primary" className="pull-right">
                  Delete
              </Button>}
      </Panel.Body>
    </Panel>
  );
};

ImageListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  owner: PropTypes.object.isRequired,
};

export default ImageListItem;
