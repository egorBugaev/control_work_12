import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import './Login.css';
import {toggleModalLogin } from "../../store/actions/images";

class Modal extends Component {

	render() {
		return (
			<Fragment>
				<div className="wrapper"
				     style={this.props.showModalLogin ? {display: 'block'} : {display: 'none'}}/>
				<div className="modal_login"
				     style={this.props.showModalLogin ? {transform: 'translateX(-50%) translateY(-50%)'} : {transform: 'translateX(-50%) translateY(-900%)'}}>
                    <img className="image" src={this.props.image} alt=""/>

                    <button type="button" onClick={this.props.toggleModalLogin}><span>close</span></button>

				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	showModalLogin: state.images.showModalLogin,
	image: state.images.currentImage
});

const mapDispatchToProps = dispatch => ({
    toggleModalLogin: () => dispatch(toggleModalLogin()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
