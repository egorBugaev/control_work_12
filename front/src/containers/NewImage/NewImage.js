import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ImageForm from "../../components/ImageForm/ImageForm";
import {createImage} from "../../store/actions/images";

class NewImage extends Component {

  createImage = ImageData => {
    this.props.onProductCreated(ImageData);
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Add Image</PageHeader>
        <ImageForm
          onSubmit={this.createImage}
          user={this.props.user}
        />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onProductCreated: imageData => {
    return dispatch(createImage(imageData))
  }
});

const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps, mapDispatchToProps)(NewImage);
