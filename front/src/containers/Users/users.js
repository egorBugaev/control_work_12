import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {deleteImage, fetchImagesId} from "../../store/actions/images";
import {Link} from "react-router-dom";

import ImageListItem from '../../components/ImageListItem/ImageListItem';

class Users extends Component {
    componentDidMount() {
        const id = this.props.location.pathname;
        this.props.onFetchImagesId(id);
    }

    render() {
        return (
            <Fragment>
                <PageHeader>
                    Images
                    { this.props.user &&
                    <Link to="/images/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add Image
                        </Button>
                    </Link>
                    }
                </PageHeader>

                {this.props.images.map(image => (
                    <ImageListItem
                        key={image._id}
                        id={image._id}
                        title={image.title}
                        owner={image.owner}
                        image={image.image}
                        user={this.props.user}
                        delete={this.props.delete}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        images: state.images.images,
        user: state.users.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchImagesId: (id) => dispatch(fetchImagesId(id)),
        delete: (id)=>dispatch(deleteImage(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
