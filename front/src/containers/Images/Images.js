import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";
import {deleteImage, fetchImages, toggleModalLogin} from "../../store/actions/images";
import {Link} from "react-router-dom";

import ImageListItem from '../../components/ImageListItem/ImageListItem';

class Images extends Component {
  componentDidMount() {
    this.props.onFetchImages();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Images
          { this.props.user &&
            <Link to="/images/new">
              <Button bsStyle="primary" className="pull-right">
                Add Image
              </Button>
            </Link>
          }
        </PageHeader>

        {this.props.images.map(image => (
          <ImageListItem
            key={image._id}
            id={image._id}
            title={image.title}
            owner={image.owner}
            image={image.image}
            user={this.props.user}
            delete={this.props.delete}
            modal={this.props.modal}
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    images: state.images.images,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchImages: () => dispatch(fetchImages()),
    delete: (id)=>dispatch(deleteImage(id)),
    modal: image =>dispatch(toggleModalLogin(image))

  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Images);
