import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import NewImage from "./containers/NewImage/NewImage";
import Register from "./containers/Register/Register";
import Images from "./containers/Images/Images";
import Login from "./containers/Login/Login";
import Users from "./containers/Users/Users";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact component={Images}/>
    <ProtectedRoute
      isAllowed={user}
      path="/images/new"
      exact
      component={NewImage}
    />
    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route path="/users" component={Users}/>
  </Switch>
);

export default Routes;
