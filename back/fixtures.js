const mongoose = require('mongoose');
const config = require('./config');
const fs = require('fs');;
const Images = require('./models/Images');
const User = require('./models/User');
const dirpath = './public/uploads';

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const files = fs.readdirSync(dirpath).sort();


db.once('open', async () => {
  try {
    await db.dropCollection('images');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

   const user =[];

    for (let i = 0 ;i < files.length; i++){

    user[i] = await User.create({
            username: 'user' + i,
            password: '123',
            role: 'user'
        });

      await Images.create({
          image: files[i],
          title: 'title',
          owner: user[i]._id,
    });
  }
  db.close();
});
