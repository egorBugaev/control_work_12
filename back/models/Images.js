const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImageSchema = new Schema({
    title: {
        type: String, required: true
    },
    image: String,
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

const Images = mongoose.model('Images', ImageSchema);

module.exports = Images;
