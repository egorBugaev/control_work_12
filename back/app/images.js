const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Image = require('../models/Images');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {

  router.get('/', (req, res) => {
    Image.find().populate('owner')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });


  router.post('/', [auth, upload.single('image')], (req, res) => {
    const imageData = req.body;

    if (req.file) {
      imageData.image = req.file.filename;
    } else {
      imageData.image = null;
    }

    const image = new Image(imageData);

    image.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/:id', (req, res) => {
    const id = req.params.id;
   Image.findOne({_id: new ObjectId(req.params.id)})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });
  router.delete('/:id', auth, (req, res) => {
    const id = req.params.id;
        Image.findByIdAndRemove(id, function(err1, doc1) { // doc here is actually er
            }).then(
            Image.find().populate('owner')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500))
        )
  });

  return router;
};

module.exports = createRouter;
