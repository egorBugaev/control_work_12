const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'image'
  },
  jwt: {
    secret: 'verySecretKey',
    expires: 3600
  },
  facebook: {
    appId: "178739386123210", // Enter your app ID here
    appSecret: "11fb490ea906a6b783b96eefcab3e8f7" // Enter your app secret here
  }
};

